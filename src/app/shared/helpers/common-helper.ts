

export class CommonHelper {

  public static isEmptyObject(obj) {
    return (Object.getOwnPropertyNames(obj).length === 0);
  }
  
};

