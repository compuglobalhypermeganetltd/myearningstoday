export const environment = {
  production: true,
  appTitle: "My Earnings Today",
  appDescription: "Calculate how much you have already earned today and compare with others",
  logoUrl: "http://www.myearningstoday.com/assets/logo.png",
  localStoragePrefix: "my-earnings-today_",
  compareToolMaxPersons: 5,
  creatorProfileLink: "https://www.linkedin.com/in/marcosgimeno/"
};
